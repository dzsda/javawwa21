package pl.sdacademy.restexample;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {
    private List<Person> people;

    public PersonController() {
        people = new ArrayList<>(Arrays.asList(
                new Person("Dariusz", "Zarzycki"),
                new Person("Damian", "Kowalski"),
                new Person("Anna", "Nowak")
        ));
    }

    @GetMapping
    public List<Person> getAll() {
        return people;
    }

    @PostMapping
    public void post(@RequestBody Person person) {
        people.add(person);
    }
}
