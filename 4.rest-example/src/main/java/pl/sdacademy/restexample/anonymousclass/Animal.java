package pl.sdacademy.restexample.anonymousclass;

public class Animal {
    private String specie;
    private int weight;

    public Animal(String specie, int weight) {
        this.specie = specie;
        this.weight = weight;
    }

    public String getSpecie() {
        return specie;
    }

    public int getWeight() {
        return weight;
    }
}
