package pl.sdacademy;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        // W przykładzie root jest typu VBox (który dziedziczy po parent).
        // Jest to korzeń ładowanego fxmla.
        Parent root = FXMLLoader.load(this.getClass().getResource("/view.fxml"));
        stage.setScene(new Scene(root));
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}
