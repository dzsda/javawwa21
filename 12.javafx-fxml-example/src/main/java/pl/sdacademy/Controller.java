package pl.sdacademy;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    private Button myButton;
    @FXML
    private CheckBox myCheckBox;
    @FXML
    private TextField myTextField;
    @FXML
    private Label myLabel;

    private int clickCount;

    public void initialize() {
        myLabel.setText(Integer.toString(clickCount));

        myButton.setOnAction(actionEvent -> {
            clickCount++;
            myLabel.setText(Integer.toString(clickCount));
        });
    }
}
