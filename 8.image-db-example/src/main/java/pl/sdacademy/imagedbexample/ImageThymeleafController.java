package pl.sdacademy.imagedbexample;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/image-th")
public class ImageThymeleafController {
    private ImageRepository imageRepository;

    public ImageThymeleafController(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @GetMapping("/add")
    public String getForm() {
        return "add-image";
    }

    @PostMapping("/add")
    public String post(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        byte[] bytes = multipartFile.getBytes();
        String fileName = multipartFile.getOriginalFilename();
        String contentType = multipartFile.getContentType();
        Image image = new Image(bytes, fileName, contentType);
        imageRepository.save(image);
        return "redirect:all";
    }

    @GetMapping("/{id}/data")
    public ResponseEntity<byte[]> getData(@PathVariable int id) {
        Image image = imageRepository.findById(id)
                .orElseThrow(EntityNotFoundException::new);
        byte[] data = image.getData();
        String contentType = image.getContentType();
        // konfigurujemy odpowiedź na żadanie:
        // za pomocą metody ok ustawiamy status 200
        return ResponseEntity.ok()
                // ustawiamy wartość nagłówka Content-Type
                .contentType(MediaType.valueOf(contentType))
                // dołączamy dane do odpowiedzi (ustawiamy ciało odpowiedzi)
                .body(data);
    }

    @GetMapping("/all")
    public String getAll(Model model) {
        List<Integer> imageIds = imageRepository.getAllIds();
        model.addAttribute("imageIds", imageIds);
        return "all-images";
    }
}
