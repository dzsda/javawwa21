package pl.sdacademy.imagedbexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImageDbExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImageDbExampleApplication.class, args);
    }

}
