package pl.sdacademy.imagedbexample;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/image")
public class ImageController {
     private ImageRepository imageRepository;

     public ImageController(ImageRepository imageRepository) {
          this.imageRepository = imageRepository;
     }

     @PostMapping
     public Image post(@RequestParam("file") MultipartFile multipartFile) throws IOException {
          byte[] bytes = multipartFile.getBytes();
          String fileName = multipartFile.getOriginalFilename();
          String contentType = multipartFile.getContentType();
          Image image = new Image(bytes, fileName, contentType);
          return imageRepository.save(image);
     }

     @GetMapping("/{id}")
     public Image get(@PathVariable int id) {
          return imageRepository.findById(id)
               .orElseThrow(EntityNotFoundException::new);
//          return imageRepository.getOne(id);
     }

     @GetMapping
     public List<Image> getAll() {
          return imageRepository.findAll();
     }

     @GetMapping("/{id}/data")
     public ResponseEntity<byte[]> getData(@PathVariable int id) {
          Image image = imageRepository.findById(id)
                  .orElseThrow(EntityNotFoundException::new);
          byte[] data = image.getData();
          String contentType = image.getContentType();
          // konfigurujemy odpowiedź na żadanie:
          // za pomocą metody ok ustawiamy status 200
          return ResponseEntity.ok()
                  // ustawiamy wartość nagłówka Content-Type
                  .contentType(MediaType.valueOf(contentType))
                  // dołączamy dane do odpowiedzi (ustawiamy ciało odpowiedzi)
                  .body(data);
     }
}
