package pl.sdacademy.imagedbexample.przykladwiazaniaobrazu;

import org.springframework.stereotype.Component;
import pl.sdacademy.imagedbexample.Image;
import pl.sdacademy.imagedbexample.ImageRepository;

import javax.annotation.PostConstruct;

@Component
public class InitBean {
    private ImageRepository imageRepository;
    private PersonRepository personRepository;

    public InitBean(ImageRepository imageRepository, PersonRepository personRepository) {
        this.imageRepository = imageRepository;
        this.personRepository = personRepository;
    }

    @PostConstruct
    public void init() {
        Image image = new Image(new byte[] {3, 1, 54}, "abc", "xyz");
        imageRepository.save(image);
        Person person = new Person("A", "B", image);
        personRepository.save(person);
    }
}
