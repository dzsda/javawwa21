package pl.sdacademy.imagedbexample.przykladwiazaniaobrazu;

import pl.sdacademy.imagedbexample.Image;

import javax.persistence.*;

@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String firstName;
    private String lastName;
    @ManyToOne
    private Image image;

    public Person() {
    }

    public Person(String firstName, String lastName, Image image) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Image getImage() {
        return image;
    }
}
