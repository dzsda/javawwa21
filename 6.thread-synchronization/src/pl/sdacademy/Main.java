package pl.sdacademy;

public class Main {
    public static void main(String[] args) {
        Bench bench = new Bench(1);
        Thread thread1 = new Thread(() -> {
            bench.takeASeat("Darek");
        });
        Thread thread2 = new Thread(() -> {
            bench.takeASeat("Tomasz");
        });
        thread1.start();
        thread2.start();
    }
}
