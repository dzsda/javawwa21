package pl.sdacademy;

public class Bench {
    private int availableSeats;

    public Bench(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public synchronized void takeASeat(String name) {
        System.out.println("Na ławce próbuje usiąść " + name);
        if (availableSeats > 0) {
            System.out.println(name + " siada na ławce");
            availableSeats--;
            System.out.println("Po zajęciu miejsca przez " + name +
                    " zostało wolnych " + availableSeats + " miejsc.");
        } else {
            System.out.println("Brak miejsca dla " + name);
        }
    }
}
