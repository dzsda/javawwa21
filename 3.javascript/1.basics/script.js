console.log("Operator typeof");
console.log(typeof "abc");
console.log(typeof 'abc');
console.log(typeof `abc`);
console.log(typeof 123);
console.log(typeof 123.321);
console.log(typeof true);
console.log(typeof [2, 3, 1]);
console.log(typeof {a: 123, b: "abc"});
console.log(typeof null);

// if (5 > 2) {
//     for (let i = 0; i < 10; i++) {
//         console.log(i);
//     }
// }

console.log("\nZakres zmiennej");
let x = 5;
console.log(x);
{
    let x = 7;
    console.log(x);
}
console.log(x);

console.log("\nTablice");
let array = [4, 13, 2, 10];
console.log(array[0]);
console.log(array.length);
array[1] = 100;
array.push(43);
// v1:
array.forEach(element => console.log(element));
// v2:
// array.forEach(function (element) {
//     console.log(element);
// });

console.log("\nObiekty");
let cat = {
    name: "Mruczek",
    color: "Biały",
    age: 3
};
console.log(cat.name);
console.log(cat.weight);

console.log("\nFunkcje");
function add(a, b) {
    console.log("Wywołano funkcję add z argumentami", arguments);
    return a + b;
}

console.log(add(3, 5));
console.log(add("a", "b"));
console.log(add(3, 4, 5));
console.log(add());
console.log(typeof add);
