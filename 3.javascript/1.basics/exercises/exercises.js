// JS.4 Napisz funkcję js4, która przyjmie trzy argumenty - tablicę, liczbę oraz łańcuch znaków.
// Funkcja zwróci odpowiedź na pytanie, czy na wskazanym przez drugi argument miejscu w tablicy
// przekazanej jako pierwszy argument, znajduje się trzeci argument.
function js4(array, index, element) {
    if (!Array.isArray(array)) {
        console.log("Pierwszy argument nie jest tablicą");
        return;
    }
    if (typeof index !== "number") {
        console.log("Drugi argument nie jest wartością liczbową");
        return;
    }
    // v1:
    // if (array[index] === element) {
    //     return true;
    // } else {
    //     return false;
    // }
    // v2:
    return array[index] === element;
}

console.log("Zadanie 4");
console.log(js4([3, "abc", 543, "xyz"], 2, 543));

// JS.5 Napisz funkcję js5, która przyjmie dwa argumenty. Pierwszy argument musi być tablicą
// (sprawdzamy to). Drugi argument jest opcjonalny - jeśli zostanie przekazany i będzie liczbą,
// to wypisujemy wartości pierwszego argumentu począwszy od indeksu równego drugiemu argumentowi.
// Jeśli drugi argument nie jest przekazany, to wypisujemy wszystkie elementy tablicy.
function js5(array, startIndex) {
    if (!Array.isArray(array)) {
        console.log("Pierwszy argument nie jest tablicą");
        return;
    }
    if (typeof startIndex !== "number") {
        startIndex = 0;
    }
    for (let i = startIndex; i < array.length; i++) {
        console.log(array[i]);
    }
}

console.log("\nZadanie 5");
// js5(["abc", "xyz", "xxxx", "afrg"]);
js5(["abc", "xyz", "xxxx", "afrg"], 2);

// JS.7 Stwórz tablicę obiektów o strukturze:
// {
//     id: x,
//     name: y,
//     value: z
// }
// Napisz funkcję js7, która przyjmie jako argument tablicę takich obiektów,
// a która zwróci średnią wartości (value) obiektów o nazwie (name) "abc" argumentu.
// Do rozwiązania użyj funkcji: filter, map oraz reduce.

function js7(array) {

    let values = array.filter(element => element.name === "abc")
        .map(element => element.value);
    let sum = values.reduce((previousValue, currentValue) => previousValue + currentValue);
    return sum / values.length;
}

let array = [
    {
        id: 2,
        name: "aaa",
        value: 343
    },
    {
        id: 5,
        name: "abc",
        value: 123
    },
    {
        id: 7,
        name: "aaa",
        value: 343
    },
    {
        id: 9,
        name: "abc",
        value: 66
    }
];
console.log("\nZadanie 7");
console.log(js7(array));
