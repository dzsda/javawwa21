// FORM.2 Dołącz do dokumentu skrypt JS. Po kliknięciu przycisku "dodaj" powinien zostać utworzony obiekt
// z właściwościami zgodnymi z nazwami i wartościami pól. Obiekt powinien zostać wypisany w konsoli.
// FORM.3 Zadeklaruj zmienną people - tablicę (na starcie pustą). Po kliknięciu przycisku "dodaj" utworzony
// w zadaniu FORM.2 obiekt powinien zostać dodatkowo dodany do tablicy people.
// FORM.4 funkcja obsługująca kliknięcie przycisku "dodaj" powinna sprawić, aby do tabeli trafił nowy wiersz,
// zawierający dane utworzonej osoby. Niech używa do tego funkcji refreshTable (którą sami stworzymy) - funkcji,
// która dla każdej osoby tworzy wiersz i wstawia go do tabeli.
// FORM.5 Dodaj dodatkową kolumnę z nagłówkiem "usuń". W wierszach powinien pojawić się przycisk
// "usuń". Przycisk będzie służył do usuwania wybranej osoby - powinna zostać usunięta zarówno
// z listy osób, jak z tabeli.
let people = [];

let createPersonForm = document.querySelector("#createPersonForm");
let firstNameInput = document.querySelector("#firstName");
let lastNameInput = document.querySelector("#lastName");
let ageInput = document.querySelector("#age");
let tableBody = document.querySelector("tbody");

createPersonForm.addEventListener("submit", function (event) {
    event.preventDefault();
    let firstName = firstNameInput.value;
    let lastName = lastNameInput.value;
    let age = ageInput.value;
    // v1:
    let person = {
        firstName: firstName,
        lastName: lastName,
        age: age
    };
    // v2:
    // let person = {
    //     firstName,
    //     lastName,
    //     age
    // }
    people.push(person);
    refreshTable();
});

function refreshTable() {
    tableBody.innerHTML = "";
    people.forEach(person => {
        let rowElement = document.createElement("tr");
        // rowElement.innerHTML = "<td>" + person.firstName + "</td>" +
        //     "<td>" + person.lastName +"</td>" +
        //     "<td>" + person.age + "</td>";
        rowElement.innerHTML = `<td>${person.firstName}</td>
            <td>${person.lastName}</td>
            <td>${person.age}</td>
            <td><input type="button" value="Usuń" class="removeButton"></td>`;
        let removeButton = rowElement.querySelector(".removeButton");
        removeButton.addEventListener("click", function () {
            people = people.filter(p => p !== person);
            refreshTable();
        });
        tableBody.appendChild(rowElement);
    });
}
