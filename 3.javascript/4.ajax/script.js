console.log("Wysyłam żądanie");
fetch("https://jsonplaceholder.typicode.com/posts")
    .then(response => response.json())
    .then(posts => console.log(posts));
console.log("Po wysyłce żądania");

let post = {
    userId: 3,
    title: "Tytuł posta",
    body: "Treść posta"
};
fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "post",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(post)
}).then(response => response.json())
    .then(post => console.log(post));
