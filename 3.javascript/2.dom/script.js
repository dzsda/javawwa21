console.log(document);
console.dir(document);

let myDiv = document.querySelector("#myDiv");
console.log(myDiv);
myDiv.setAttribute("abc", "xyz");
myDiv.style.backgroundColor = "cyan";
myDiv.classList.add("my-class");
myDiv.innerText = "Nowa treść diva";
// myDiv.innerHTML = "<p>Nowe dziecko</p>";

let newElement = document.createElement("a");
newElement.setAttribute("href", "https://google.com");
newElement.innerText = "Google.com";

myDiv.appendChild(newElement);
