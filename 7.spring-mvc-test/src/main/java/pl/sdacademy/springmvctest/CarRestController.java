package pl.sdacademy.springmvctest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car-rest")
public class CarRestController {
    private CarRepository carRepository;

    public CarRestController(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @GetMapping
    public List<Car> getAll() {
        return carRepository.findAll();
    }

    @GetMapping("/{id}")
    public Car get(@PathVariable int id) {
        return carRepository.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Car post(@RequestBody Car car) {
        return carRepository.save(car);
    }
}
