package pl.sdacademy.springmvctest;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class CarRepository {
    private List<Car> cars;

    public CarRepository() {
        cars = new ArrayList<>(Arrays.asList(
                new Car(1, "Fiat", "Panda"),
                new Car(2, "Fiat", "Punto"),
                new Car(3, "Citroen", "C3")
        ));
    }

    public Car findById(int id) {
        return cars.stream()
                .filter(c -> c.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public List<Car> findAll() {
        return cars;
    }

    public Car save(Car car) {
        int id = cars.stream()
                .mapToInt(Car::getId)
                .max()
                .orElse(0) + 1;
        car.setId(id);
        cars.add(car);
        return car;
    }
}
