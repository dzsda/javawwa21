package pl.sdacademy.springmvctest;

public class Car {
    private Integer id;
    private String make;
    private String model;

    public Car(Integer id, String make, String model) {
        this.id = id;
        this.make = make;
        this.model = model;
    }

    public Integer getId() {
        return id;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
