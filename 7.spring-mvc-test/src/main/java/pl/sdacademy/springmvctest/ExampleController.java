package pl.sdacademy.springmvctest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/example-thymeleaf")
public class ExampleController {
    @GetMapping
    public String example1(Model model) {
        model.addAttribute("myAttribute", "my-value");
        return "example1";
    }

    @PostMapping
    public String example2(Car car) {
        return "example1";
    }
}
