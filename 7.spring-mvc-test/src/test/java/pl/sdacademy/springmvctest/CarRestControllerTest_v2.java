package pl.sdacademy.springmvctest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
// v2 z mockowaniem repozytorium
public class CarRestControllerTest_v2 {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    CarRepository carRepository;

    @Test
    void shouldReturnPreparedCar() throws Exception {
        when(carRepository.findById(eq(10)))
                .thenReturn(new Car(10, "Ford", "Mustang"));

        mockMvc.perform(MockMvcRequestBuilders.get("/car-rest/10"))
                // czy kod statusu to 200 (OK)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(10))
                .andExpect(jsonPath("$.make").value("Ford"))
                .andExpect(jsonPath("$.model").value("Mustang"));
    }

    @Test
    void shouldCreateCar() throws Exception {
        when(carRepository.save(any()))
                .thenReturn(new Car(100, null, null));

        mockMvc.perform(MockMvcRequestBuilders.post("/car-rest")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"make\":\"abc\", \"model\":\"xyz\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(100));

        // sprawdzenie, czy kontroler wywołał metodę save repozytorium
        verify(carRepository).save(any());
    }
}
