package pl.sdacademy.springmvctest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest
public class Example2 {
    @MockBean
    CarRepository carRepository;

    @Test
    void test() {
        Car car = new Car(10, "Ford", "Mustang");
        when(carRepository.findById(eq(10)))
                .thenReturn(car);
        assertEquals("Ford", carRepository.findById(10).getMake());
    }
}
