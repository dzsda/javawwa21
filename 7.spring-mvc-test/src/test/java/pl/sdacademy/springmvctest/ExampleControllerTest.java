package pl.sdacademy.springmvctest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ExampleControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void shouldReturnExample1ViewAndSetMyAttributeValue() throws Exception {
        mockMvc.perform(get("/example-thymeleaf"))
                .andExpect(status().isOk())
                .andExpect(view().name("example1"))
                .andExpect(model().attribute("myAttribute", "my-value"));
    }

    @Test
    void shouldReturnExample1ViewAndReceiveModelAttribute() throws Exception {
        Car car = new Car(5, "VW", "Tiguan");
        mockMvc.perform(post("/example-thymeleaf")
                .flashAttr("car", car))
                .andExpect(status().isOk())
                .andExpect(view().name("example1"));
    }
}
