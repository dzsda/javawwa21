package pl.sdacademy.springmvctest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CarRestControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void shouldReturnPreparedCar() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/car-rest/3"))
                // czy kod statusu to 200 (OK)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(3))
                .andExpect(jsonPath("$.make").value("Citroen"))
                .andExpect(jsonPath("$.model").value("C3"));
    }


}
