package pl.sdacademy.springmvctest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class Example1 {
    @Autowired
    CarRepository carRepository;

    @Test
    void name() {
        assertNotNull(carRepository);
        assertEquals(3, carRepository.findAll().size());
    }
}
