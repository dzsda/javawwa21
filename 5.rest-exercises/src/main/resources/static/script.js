let tableBody = document.querySelector("tbody");
let createCarForm = document.querySelector("form");
let makeInput = document.querySelector("#make");
let modelInput = document.querySelector("#model");
let vinInput = document.querySelector("#vin");

let apiUrl = "http://localhost:8080/car";
function reloadTable() {
    fetch(apiUrl)
        .then(response => response.json())
        .then(cars => {
            tableBody.innerHTML = "";
            cars.forEach(car => {
                let rowElement = document.createElement("tr");
                rowElement.innerHTML = `<td>${car.make}</td><td>${car.model}</td>`;
                tableBody.appendChild(rowElement);
            });
        });
}

createCarForm.addEventListener("submit", event => {
    event.preventDefault();
    let car = {
        make: makeInput.value,
        model: modelInput.value,
        vin: vinInput.value
    };
    fetch(apiUrl, {
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(car)
    }).then(response => reloadTable())
});

reloadTable();
