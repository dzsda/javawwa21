package pl.sdacademy.restexercises;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {
    private List<Car> cars;

    public CarController() {
        cars = new ArrayList<>(Arrays.asList(
            new Car(1, "Ford", "Fiesta", "afawergae"),
            new Car(2, "Ford", "Mustang", "htrhr5gawerfg"),
            new Car(3, "Fiat", "500", "2343tgWEgw")
        ));
    }

    @GetMapping
    public List<Car> getAll() {
        return cars;
    }

    @PostMapping
    public Car post(@RequestBody Car car) {
        int nextId = cars.stream()
                .mapToInt(Car::getId)
                .max()
                .orElse(0) + 1;
        car.setId(nextId);
        cars.add(car);
        return car;
    }
}
