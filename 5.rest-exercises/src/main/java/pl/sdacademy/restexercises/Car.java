package pl.sdacademy.restexercises;

public class Car {
    private Integer id;
    private String make;
    private String model;
    private String vin;

    public Car(Integer id, String make, String model, String vin) {
        this.id = id;
        this.make = make;
        this.model = model;
        this.vin = vin;
    }

    public Integer getId() {
        return id;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getVin() {
        return vin;
    }

    public void setId(int id) {
        this.id = id;
    }
}
