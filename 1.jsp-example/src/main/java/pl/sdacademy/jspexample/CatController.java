package pl.sdacademy.jspexample;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/cat")
public class CatController {
    @GetMapping
    public String getAll(Model model) {
        List<Cat> cats = Arrays.asList(
                new Cat("Mruczek", "biały", 3),
                new Cat("Stefan", "czarny", 2),
                new Cat("Elvis", "rudy", 4)
        );
        model.addAttribute("cats", cats);
        return "all-cats";
    }
}
