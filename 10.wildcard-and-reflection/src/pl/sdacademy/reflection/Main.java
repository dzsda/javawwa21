package pl.sdacademy.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {
        Class<Car> carClass = Car.class;

        Field[] fields = carClass.getDeclaredFields();
        System.out.println("\nMetody:");
        for (Field field : fields) {
            System.out.println(field.getName());
        }

        System.out.println("\nMetody:");
        Method[] methods = carClass.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("\nMetoda: " + method.getName());
            Class<?> returnType = method.getReturnType();
            System.out.println("Metoda zwraca wartość typu: " + returnType.getName());
            Class<?>[] parameterTypes = method.getParameterTypes();
            System.out.println("Typy parametrów metody:");
            for (Class<?> parameterType : parameterTypes) {
                System.out.println(parameterType.getName());
            }
        }

        Car car1 = new Car("Ford Fiesta", 123);

        Field modelField = carClass.getDeclaredField("model");
        modelField.setAccessible(true);
        String car1Model = (String) modelField.get(car1);
        System.out.println("\nWartość pola model obiektu przypisanego do zmiennej car1: " + car1Model);

        modelField.set(car1, "Fiat Panda");
        System.out.println("\nNowa wartość pola model obiektu przypisanego do zmiennej car1: " + car1.getModel());

        Method setMaxSpeedMethod = carClass.getDeclaredMethod("setMaxSpeed", int.class);
        setMaxSpeedMethod.invoke(car1, 55);
        System.out.println("\nNowa wartość pola maxSpeed obiektu przypisanego do zmiennej car1: " + car1.getMaxSpeed());

        Constructor<Car> carConstructor = carClass.getDeclaredConstructor(String.class, int.class);
        Car car2 = carConstructor.newInstance("Dacia Duster", 123);

        System.out.println(car2);
    }
}
