package pl.sdacademy.wildcard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>(Arrays.asList("ABC", "XYZ"));
        List<Integer> integerList = new ArrayList<>(Arrays.asList(123, 321));
        List<Double> doubleList = new ArrayList<>(Arrays.asList(123.1, 321.));
        List<Number> numberList = new ArrayList<>(Arrays.asList(123, 123.321));

        example1(stringList);
        example1(integerList);
        example1(doubleList);
        example1(numberList);

//        example2(stringList);
        example2(integerList);
        example2(doubleList);
        example2(numberList);

//        example3(stringList);
        example3(integerList);
//        example3(doubleList);
        example3(numberList);
    }

    private static void example1(List<?> list) {
        Object element = list.get(0);
//        list.add("abc");
//        list.add(123);
    }

    private static boolean contains(List<?> list, Object element) {
        return list.stream().anyMatch(el -> el.equals(element));
    }

    private static void example2(List<? extends Number> list) {
        Number element = list.get(0);
//        list.add(123.321);
//        list.add(123);
    }

    private static void example3(List<? super Integer> list) {
        Object element = list.get(0);
        list.add(123);
    }

}
