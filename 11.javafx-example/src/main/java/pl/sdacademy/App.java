package pl.sdacademy;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {
        VBox vBox = new VBox();
        TextField textField1 = new TextField("BBB");
        vBox.getChildren().add(textField1);
        vBox.getChildren().add(new TextField("AAA"));
        Button button1 = new Button("Przycisk 1");
        button1.setOnAction(actionEvent -> {
            System.out.println("Przycisk został kliknięty!");
            textField1.setText("Przycisk został kliknięty!");
        });
        vBox.getChildren().add(button1);
        vBox.getChildren().add(new CheckBox("Checkbox"));

        HBox hBox = new HBox();
        hBox.getChildren().add(new Button("Przycisk 2"));
        hBox.getChildren().add(new Button("Przycisk 3"));

        vBox.getChildren().add(hBox);

        stage.setTitle("Hello world!");
        stage.setScene(new Scene(vBox));
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}
