package pl.sdacademy.datajpaexample;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/cat")
public class CatController {
    private CatRepository catRepository;

    public CatController(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @GetMapping
    public String getAll(Model model) {
        List<Cat> cats = catRepository.findAll();
        model.addAttribute("cats", cats);
        return "all-cats";
    }
}
