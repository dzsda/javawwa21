package pl.sdacademy.validationexample;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/car")
public class CarController {
    @GetMapping("/add")
    public String getForm(Model model) {
        model.addAttribute("validated", false);
        model.addAttribute("car", new Car());
        return "add-car";
    }

    // Jeśli od razu po walidowanym parametrze będzie parametr typu BindingResult, to metoda zostanie wywołana,
    // a wynik walidacji możemy znaleźć w ciele obiektu tego typu (BindingResult)
    @PostMapping("/add")
    public String postForm(Model model, @Valid Car car, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("validated", true);
            return "add-car";
        }

        return "add-result";
    }
}
