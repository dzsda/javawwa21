package pl.sdacademy.validationexample;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class Car {
    @NotEmpty
    private String make;
    @Size(min = 3, max = 7)
    private String model;

    public Car() {
    }

    public Car(String model, String make) {
        this.model = model;
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public String getMake() {
        return make;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setMake(String make) {
        this.make = make;
    }
}
